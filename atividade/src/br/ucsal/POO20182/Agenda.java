package br.ucsal.POO20182;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Agenda {
    private List<Contato> agenda = new ArrayList<>();

    public boolean adicionarContato(Contato contato) {
        agenda.add(contato);
        return true;
    }

    public boolean removerContato(Contato contato) {
        for (Contato contatoatual : agenda) {
            if (!contatoatual.equals(contato)) {
                return false;
            }
        }
        agenda.remove(contato);
        return true;
    }

    public List<Contato> imprimirNomes() {
        Collections.sort(agenda, new OrdenaPorNome());
        return agenda;

    }

    public Boolean pesquisarContato(String nome) {
        for (Contato contatoatual : agenda) {
            if (contatoatual.getNome().equalsIgnoreCase(nome)) {
                System.out.println(contatoatual);
                return true;
            }
        }
        return false;

    }

}
