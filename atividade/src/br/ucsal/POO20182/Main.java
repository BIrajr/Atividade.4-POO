package br.ucsal.POO20182;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {

        Agenda agenda = new Agenda();
        agenda.adicionarContato(new Contato("Ronaldo", "33716459", "15/08/1999", "profissional"));
        agenda.adicionarContato(new Contato("Ronalo", "33716459", "15/08/1999", "pessoal"));
        agenda.adicionarContato(new Contato("Ronald", "33716459", "15/08/1999", "pessoal"));
        agenda.adicionarContato(new Contato("Ronado", "33716459", "15/08/1999", "profissional"));
        agenda.adicionarContato(new Contato("Ronldo", "33716459", "15/08/1999", "profissional"));
        agenda.adicionarContato(new Contato("Roaldo", "33716459", "15/08/1999", "profissional"));
        agenda.adicionarContato(new Contato("Rnaldo", "33716459", "15/08/1999", "pessoal"));
        agenda.adicionarContato(new Contato("Eli", "33716459", "15/08/1999", "profissional"));
        System.out.println(agenda.imprimirNomes());

    }

	
}
