package br.ucsal.POO20182;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Contato {
    private String nome;
    private String telefone;
    private Date dataNascimento;
    private TipoNomeEnum tipo;

    @SuppressWarnings("static-access")
    public Contato(String nome, String telefone, String dataNascimentoString, String teste) {

        this.nome = nome;
        this.telefone = telefone;
        this.tipo = tipo.valueOf(teste.toUpperCase());
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.dataNascimento = df.parse(dataNascimentoString);

        } catch (ParseException e) {
            System.out.println("Data inv�lida");
        }
    }

    public String getNome() {

        return nome;

    }

    public void setNome(String nome) {

        this.nome = nome;

    }

    public String getTelefone() {

        return telefone;

    }

    public void setTelefone(String telefone) {

        this.telefone = telefone;

    }

    public Date getDataNascimento() {

        return dataNascimento;

    }

    public TipoNomeEnum getTipo() {

        return tipo;

    }

    @Override
    public String toString() {
        return "Contato [nome=" + nome + ", telefone=" + telefone + ", DataNascimento=" + dataNascimento + ", tipo="
                + tipo + "]";
    }

}