package br.ucsal.POO20182;
import java.util.Comparator;

class OrdenaPorNome implements Comparator<Contato> {

    public int compare(Contato um, Contato dois) {
        return um.getNome().compareTo(dois.getNome());
    }
}

